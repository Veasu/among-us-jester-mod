﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.IL2CPP;
using BepInEx.IL2CPP.UnityEngine;
using HarmonyLib;
using Hazel;
using Il2CppDumper;
using InnerNet;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using UnhollowerBaseLib;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JesterMod
{
    [BepInPlugin("org.bepinex.plugins.JesterMod", "Jester Mod", "1.0.0.0")]
    public class JesterMod : BasePlugin
    {
        public static BepInEx.Logging.ManualLogSource log;
        private readonly Harmony harmony;

        public JesterMod()
        {
            log = Log;
            this.harmony = new Harmony("Jester Mod");

        }
        public override void Load()
        {
            log.LogMessage("Jester Mod loaded");
            this.harmony.PatchAll();
        }
    }
}



