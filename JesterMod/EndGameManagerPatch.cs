﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.IL2CPP;
using BepInEx.IL2CPP.UnityEngine;
using HarmonyLib;
using Hazel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnhollowerBaseLib;
using UnityEngine;

namespace JesterMod
{
    [HarmonyPatch]
    public static class EndGameManagerPatch
    {
        [HarmonyPatch(typeof(ABNGEPFHMHP.EHKHLOLEFFD), "MoveNext")]
        public static void Postfix(ABNGEPFHMHP.EHKHLOLEFFD __instance)
        {
            if (PlayerControlPatch.jesterExiled)
            {
                if (PlayerControlPatch.localIsJester)
                {
                    __instance.field_Public_ABNGEPFHMHP_0.WinText.Text = "Big Winner";
                    __instance.field_Public_ABNGEPFHMHP_0.WinText.Color = new Color((float)(102.0 / 255.0), 0, (float)(204.0 / 255.0), 1);
                    __instance.field_Public_ABNGEPFHMHP_0.BackgroundBar.material.color = new Color((float)(102.0 / 255.0), 0, (float)(204.0 / 255.0), 1); 
                }
                else
                {
                    JesterMod.log.LogMessage("Memes");
                    __instance.field_Public_ABNGEPFHMHP_0.WinText.Text = "Jester Wins";
                    __instance.field_Public_ABNGEPFHMHP_0.WinText.Color = new Color(1, 0, 0, 1);
                    __instance.field_Public_ABNGEPFHMHP_0.BackgroundBar.material.color = new Color(1, 0, 0, 1);
                }
            }
            else
            {
                if (PlayerControlPatch.localIsJester)
                {
                    __instance.field_Public_ABNGEPFHMHP_0.WinText.Text = "Defeat";
                    __instance.field_Public_ABNGEPFHMHP_0.WinText.Color = new Color((float)(102.0 / 255.0), 0, (float)(204.0 / 255.0), 1);
                    __instance.field_Public_ABNGEPFHMHP_0.BackgroundBar.material.color = new Color((float)(102.0 / 255.0), 0, (float)(204.0 / 255.0), 1);
                }
            }
        }
    }
}
