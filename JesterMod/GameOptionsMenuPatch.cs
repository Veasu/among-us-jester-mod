﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnhollowerBaseLib;
using UnityEngine;

namespace JesterMod
{
    [HarmonyPatch(typeof(PHCKLDDNJNP))]
    public static class GameOptionMenuPatch
    {
        public static BCLDBBKFJPK enableJester;

        [HarmonyPostfix]
        [HarmonyPatch("Start")]
        public static void Postfix1(PHCKLDDNJNP __instance)
        {
            if (GameObject.FindObjectsOfType<BCLDBBKFJPK>().Count == 4)
            {
               
                    BCLDBBKFJPK showAnonymousvote = GameObject.FindObjectsOfType<BCLDBBKFJPK>().ToList().Where(x => x.TitleText.Text == "Anonymous Votes").First();
                    enableJester = GameObject.Instantiate(showAnonymousvote);

                    enableJester.TitleText.Text = "Enable Jester";

                    enableJester.NHLMDAOEOAE = CustomGameOptions.enableJester;
                    enableJester.CheckMark.enabled = CustomGameOptions.enableJester;

                    LLKOLCLGCBD[] options = new LLKOLCLGCBD[__instance.KJFHAPEDEBH.Count + 1];
                    __instance.KJFHAPEDEBH.ToArray().CopyTo(options, 0);
                    options[options.Length - 1] = enableJester;
                    __instance.KJFHAPEDEBH = new Il2CppReferenceArray<LLKOLCLGCBD>(options);
                
            }
        }

        [HarmonyPostfix]
        [HarmonyPatch("Update")]
        public static void Postfix2(PHCKLDDNJNP __instance)
        {
            BCLDBBKFJPK showAnonymousvote = GameObject.FindObjectsOfType<BCLDBBKFJPK>().ToList().Where(x => x.TitleText.Text == "Anonymous Votes").First();
            if (enableJester){
                enableJester.transform.position = showAnonymousvote.transform.position - new Vector3(0, 5.5f, 0);
            }

        }
    }

    [HarmonyPatch]
    public static class ToggleButtonPatch
    {
        [HarmonyPatch(typeof(BCLDBBKFJPK), "Toggle")]
        public static bool Prefix(BCLDBBKFJPK __instance)
        {

            if (__instance.TitleText.Text == "Enable Jester")
            {
                CustomGameOptions.enableJester = !CustomGameOptions.enableJester;
                FFGALNAPKCD.LocalPlayer.RpcSyncSettings(FFGALNAPKCD.GameOptions);

                __instance.NHLMDAOEOAE = CustomGameOptions.enableJester;
                __instance.CheckMark.enabled = CustomGameOptions.enableJester;
                return false;

            }
            return true;

        }

    }
}

