﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.IL2CPP;
using BepInEx.IL2CPP.UnityEngine;
using HarmonyLib;
using Hazel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnhollowerBaseLib;
using UnityEngine;

namespace JesterMod
{
    enum CustomRPC
    {
        SetJester = 40,
        SyncCustomSettings = 41,
        JesterDead = 42
    }
    enum RPC
    {
        PlayAnimation = 0,
        CompleteTask = 1,
        SyncSettings = 2,
        SetInfected = 3,
        Exiled = 4,
        CheckName = 5,
        SetName = 6,
        CheckColor = 7,
        SetColor = 8,
        SetHat = 9,
        SetSkin = 10,
        ReportDeadBody = 11,
        MurderPlayer = 12,
        SendChat = 13,
        StartMeeting = 14,
        SetScanner = 15,
        SendChatNote = 16,
        SetPet = 17,
        SetStartCounter = 18,
        EnterVent = 19,
        ExitVent = 20,
        SnapTo = 21,
        Close = 22,
        VotingComplete = 23,
        CastVote = 24,
        ClearVote = 25,
        AddVote = 26,
        CloseDoorsOfType = 27,
        RepairSystem = 28,
        SetTasks = 29,
        UpdateGameData = 30,
    }

    [HarmonyPatch]
    public static class PlayerControlPatch
    {
        public static FFGALNAPKCD[] closestPlayers = new FFGALNAPKCD[2];
        public static FFGALNAPKCD Jester;
        public static bool jesterExiled;
        public static bool localIsJester;
        public static DateTime lastKilled;
        [HarmonyPatch(typeof(FFGALNAPKCD), "HandleRpc")]
        public static void Postfix(byte HKHMBLJFLMC, MessageReader ALMCIJKELCP)
        {
            try
            {
                switch (HKHMBLJFLMC)
                {
                    case (byte)CustomRPC.SetJester:
                        {
                            byte JesterID = ALMCIJKELCP.ReadByte();
                            foreach (FFGALNAPKCD player in FFGALNAPKCD.AllPlayerControls)
                            {
                                if (player.PlayerId == JesterID)
                                {
                                    Jester = player;
                                }
                            }
                            break;
                        }
                    case (byte)CustomRPC.SyncCustomSettings:
                        {
                            CustomGameOptions.enableJester = ALMCIJKELCP.ReadBoolean();
                            break;
                        }
                    case (byte)CustomRPC.JesterDead:
                        {
                            jesterExiled = true;

                            HLBNNHFCNAJ.PLBGOMIEONF(AIMMJPEOPEC.HumansByTask, false);
                            break;
                        }
                }
            }
            catch (Exception e) {
                JesterMod.log.LogInfo("RPC error... possible reasons: Not all players in the lobby have installed the mod or Jester mod versions do not match");
            }
        }

        public static bool isJester(FFGALNAPKCD player)
        {
            if (Jester == null) return false;
            return player.PlayerId == Jester.PlayerId;
        }

        public static FFGALNAPKCD getPlayerById(byte id)
        {
            foreach (FFGALNAPKCD player in FFGALNAPKCD.AllPlayerControls)
            {
                if (player.PlayerId == id)
                {
                    return player;
                }
            }
            return null;
        }

        public static FFGALNAPKCD getJester()
        {
            if (Jester == null) return null;
            return Jester;
        }

        public static List<string> getImpostersNames()
        {
            List<string> ImpNames = new List<string>();
            foreach (FFGALNAPKCD player in FFGALNAPKCD.AllPlayerControls)
            {
                if (player.NDGFFHMFGIG.DLPCKPBIJOE)
                {
                    ImpNames.Add(player.name);
                }
            }
            return ImpNames;
        }

        public static FFGALNAPKCD[] getClosestPlayers(FFGALNAPKCD refplayer)
        {
            double mindist = double.MaxValue;
            FFGALNAPKCD[] closestplayers = new FFGALNAPKCD[2];
            closestplayers[0] = null;
            closestplayers[1] = null;
            foreach (FFGALNAPKCD player in FFGALNAPKCD.AllPlayerControls)
            {
                if (player.NDGFFHMFGIG.DLPCKPBIJOE) continue;
                if (player.PlayerId != refplayer.PlayerId)
                {
                    double dist = getDistBetweenPlayers(player, refplayer);
                    if (dist < mindist)
                    {
                        mindist = dist;
                        if (closestplayers[0] != null)
                        {
                            closestplayers[1] = closestplayers[0];
                        }
                        closestplayers[0] = player;
                    }
                }
            }

            return closestplayers;
        }

        public static double getDistBetweenPlayers(FFGALNAPKCD player, FFGALNAPKCD refplayer)
        {
            var refpos = refplayer.GetTruePosition();
            var playerpos = player.GetTruePosition();
            return Math.Sqrt((refpos[0] - playerpos[0]) * (refpos[0] - playerpos[0]) + (refpos[1] - playerpos[1]) * (refpos[1] - playerpos[1]));
        }

        public static List<FFGALNAPKCD> getCrewMates(Il2CppReferenceArray<EGLJNOMOGNP.DCJMABDDJCF> infection)
        {

            List<FFGALNAPKCD> CrewmateIds = new List<FFGALNAPKCD>();
            foreach (FFGALNAPKCD player in FFGALNAPKCD.AllPlayerControls)
            {

                bool isInfected = false;
                foreach (EGLJNOMOGNP.DCJMABDDJCF infected in infection)
                {

                    if (player.PlayerId == infected.LAOEJKHLKAI.PlayerId)
                    {
                        isInfected = true;

                        break;
                    }

                }
                if (!isInfected)
                {
                    CrewmateIds.Add(player);
                }


            }
            return CrewmateIds;

        }

        [HarmonyPatch(typeof(FFGALNAPKCD), "Exiled")]
        public static void Postfix()
        {
            if (isJester(FFGALNAPKCD.LocalPlayer))
            {
                jesterExiled = true;

                MessageWriter writer = FMLLKEACGIO.Instance.StartRpcImmediately(FFGALNAPKCD.LocalPlayer.NetId, (byte)CustomRPC.JesterDead, Hazel.SendOption.None, -1);

                byte JesterID = Jester.PlayerId;
                writer.Write(JesterID);

                FMLLKEACGIO.Instance.FinishRpcImmediately(writer);

                HLBNNHFCNAJ.PLBGOMIEONF(AIMMJPEOPEC.HumansByTask, false);
            }
        }

        [HarmonyPatch(typeof(FFGALNAPKCD), "RpcSetInfected")]
        public static void Postfix(Il2CppReferenceArray<EGLJNOMOGNP.DCJMABDDJCF> JPGEIBIBJPJ)
        {

            MessageWriter writer = FMLLKEACGIO.Instance.StartRpcImmediately(FFGALNAPKCD.LocalPlayer.NetId, (byte)CustomRPC.SetJester, Hazel.SendOption.None, -1);
            List<FFGALNAPKCD> crewmates = getCrewMates(JPGEIBIBJPJ);

            System.Random r = new System.Random();
            Jester = crewmates[r.Next(0, crewmates.Count)];
            byte JesterID = Jester.PlayerId;

            writer.Write(JesterID);

            FMLLKEACGIO.Instance.FinishRpcImmediately(writer);
        }


        [HarmonyPatch(typeof(FFGALNAPKCD), "RpcSyncSettings")]
        public static void Postfix(KMOGFLPJLLK IOFBPLNIJIC)
        {
            if (FFGALNAPKCD.AllPlayerControls.Count > 1)
            {
                MessageWriter writer = FMLLKEACGIO.Instance.StartRpcImmediately(FFGALNAPKCD.LocalPlayer.NetId, (byte)CustomRPC.SyncCustomSettings, Hazel.SendOption.None, -1);

                writer.Write(CustomGameOptions.enableJester);
                FMLLKEACGIO.Instance.FinishRpcImmediately(writer);
            }

        }


        [HarmonyPatch(typeof(FFGALNAPKCD), "FixedUpdate")]
        public static void Postfix(FFGALNAPKCD __instance)
        {
            if (isJester(FFGALNAPKCD.LocalPlayer))
            {
                localIsJester = true;

                foreach (PILBGHDHJLH task in FFGALNAPKCD.LocalPlayer.myTasks)
                {
                    if (!task.NCEPDPNKMFC)
                    {
                        MessageWriter writer = FMLLKEACGIO.Instance.StartRpcImmediately(FFGALNAPKCD.LocalPlayer.NetId, (byte)RPC.CompleteTask, Hazel.SendOption.None, -1);

                        writer.Write(task.DKHNENPOEOF);

                        FMLLKEACGIO.Instance.FinishRpcImmediately(writer);

                        HLBNNHFCNAJ.PLBGOMIEONF(AIMMJPEOPEC.HumansByTask, false);
                        task.Complete();
                    }
                }
            }
        }
    }
}
