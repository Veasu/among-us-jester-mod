﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;



namespace JesterMod
{
    [HarmonyPatch]
    public static class HudManagerPatch
    {
        // Methods

        static int counter = 0;

        public static MLPJGKEACMM KillButton = null;
        static System.Random random = new System.Random();
        static string GameSettingsText = null;

        public static void UpdateGameSettingsText(PIEFJFEOGOL __instance)
        {
            if (__instance.GameSettings.Text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Count() == 19)
            {
                GameSettingsText = __instance.GameSettings.Text;
            }
            if (GameSettingsText != null)
            {
                if (CustomGameOptions.enableJester)
                    __instance.GameSettings.Text = GameSettingsText + "Enable Jester: On" + "\n";
                else
                    __instance.GameSettings.Text = GameSettingsText + "Enable Jester: Off" + "\n";
            }

        }


        public static void updateGameOptions(KMOGFLPJLLK options)
        {
            Il2CppSystem.Collections.Generic.List<FFGALNAPKCD> allplayer = FFGALNAPKCD.AllPlayerControls;

            foreach (FFGALNAPKCD player in allplayer)
            {
                player.RpcSyncSettings(options);
            }

        }

        public static void updateOOCJALPKPEP(OOCJALPKPEP __instance)
        {
            foreach (HDJGDMFCHDN HDJGDMFCHDN in __instance.HBDFFAHBIGI)
            {
                if (HDJGDMFCHDN.NameText.Text == PlayerControlPatch.Jester.name)
                {
                    if (PlayerControlPatch.isJester(FFGALNAPKCD.LocalPlayer) || FFGALNAPKCD.LocalPlayer.NDGFFHMFGIG.DAPKNDBLKIA)
                    {
                        HDJGDMFCHDN.NameText.Color = new Color((float)(102.0 / 255.0), 0, (float)(204.0 / 255.0), 1);
                    }
                }

                if (FFGALNAPKCD.LocalPlayer.NDGFFHMFGIG.DLPCKPBIJOE)
                {
                    foreach (string name in PlayerControlPatch.getImpostersNames())
                    {
                        if (HDJGDMFCHDN.name == name)
                        {
                            HDJGDMFCHDN.NameText.Color = new Color(1, 0, 0, 1);
                        }
                    }
                }
            }
        }

        [HarmonyPatch(typeof(PIEFJFEOGOL), "Update")]
        public static void Postfix(PIEFJFEOGOL __instance)
        {
            KillButton = __instance.KillButton;
            if (OOCJALPKPEP.Instance != null)
            {
                updateOOCJALPKPEP(OOCJALPKPEP.Instance);
            }

            UpdateGameSettingsText(__instance);

            if (FFGALNAPKCD.AllPlayerControls.Count > 1 & PlayerControlPatch.Jester != null)
            {
                if (PlayerControlPatch.isJester(FFGALNAPKCD.LocalPlayer))
                {
                    FFGALNAPKCD.LocalPlayer.nameText.Color = new Color((float)(102.0 / 255.0), 0, (float)(204.0 / 255.0), 1);

                    Il2CppSystem.Collections.Generic.List<FFGALNAPKCD> allplayer = FFGALNAPKCD.AllPlayerControls;
                }

                if (FFGALNAPKCD.LocalPlayer.NDGFFHMFGIG.DAPKNDBLKIA)
                {
                    PlayerControlPatch.Jester.nameText.Color = new Color((float)(102.0 / 255.0), 0, (float)(204.0 / 255.0), 1);

                    FFGALNAPKCD[] closestPlayers = PlayerControlPatch.getClosestPlayers(FFGALNAPKCD.LocalPlayer);

                    if (closestPlayers[0] != null)
                    {
                        if (PlayerControlPatch.isJester(closestPlayers[0]))
                        {
                            if (closestPlayers[1] != null)
                            {
                                double dist = PlayerControlPatch.getDistBetweenPlayers(FFGALNAPKCD.LocalPlayer, closestPlayers[1]);
                                if (dist > KMOGFLPJLLK.JMLGACIOLIK[FFGALNAPKCD.GameOptions.DLIBONBKPKL])
                                {
                                    KillButton.gameObject.SetActive(false);
                                    KillButton.isActive = false;
                                }
                                else
                                {
                                    KillButton.SetTarget(closestPlayers[1]);
                                    KillButton.gameObject.SetActive(true);
                                    KillButton.isActive = true;
                                }
                            }
                            else
                            {
                                KillButton.gameObject.SetActive(false);
                                KillButton.isActive = false;
                            }
                        }
                        else if(closestPlayers[1] != null)
                        {
                            KillButton.SetTarget(closestPlayers[1]);
                            KillButton.gameObject.SetActive(true);
                            KillButton.isActive = true;
                        }
                    }
                }
            }

            if (GameOptionMenuPatch.enableJester != null)
            {
                var isOptionsMenuActive = GameObject.FindObjectsOfType<PHCKLDDNJNP>().Count != 0;
                GameOptionMenuPatch.enableJester.gameObject.SetActive(isOptionsMenuActive);
            }
        }
    }
}
