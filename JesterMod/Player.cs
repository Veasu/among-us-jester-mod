﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


namespace JesterMod
{
    public class Player
    {
        public FFGALNAPKCD playerdata;
        public bool isJester;

        public Player(FFGALNAPKCD playerdata)
        {
            this.playerdata = playerdata;
            isJester = false;
        }
        public void Update()
        {
            if (isJester)
            {
                JesterMod.log.LogMessage("Memes");
                playerdata.nameText.Color = new Color((float)(102.0 / 255.0), 0, (float)(204.0 / 255.0), 1);
            }
        }
    }
}
